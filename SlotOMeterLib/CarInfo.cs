﻿//*************************************************************************************************
//* SlotOMeter - Monitor for slotracing with Carrera digital interface
//* Copyright (C) 2015-2012 by Linus & Joerg Plenert
//*************************************************************************************************
//* Main project: SlotOMeter
//* Sub project :
//*************************************************************************************************
//* This program is free software; you can redistribute it and/or modify
//* it under the terms of the GNU General Public License as published by
//* the Free Software Foundation; either version 2 of the License, or
//* (at your option) any later version.
//*
//* This program is distributed in the hope that it will be useful,
//* but WITHOUT ANY WARRANTY; without even the implied warranty of
//* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//* GNU General Public License for more details.
//*
//* You should have received a copy of the GNU General Public License
//* along with this program; if not, write to the Free Software
//* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//*************************************************************************************************
//* E-Mail / Internet							Snail-Mail
//* Joerg@plenert.net							Joerg Plenert, Roenskenstrasse 69
//* http://www.plenert.net						46562 Voerde, Germany
using System;

namespace SlotOMeter
{
	/// <summary>
	/// Information about one (slot)car
	/// </summary>
	public class CarInfo
	{
		public BWValue Laps { get; protected set; }
		public BWValue FastestLapTime { get; protected set; }
		public BWValue SlowestLapTime { get; protected set; }
		public BWValue LastLapTime { get; protected set; }
		public BWValue AverageLapTime { get; protected set; }
		public BWValue TotalLapsTime { get; protected set; }
		public int Rank { get; set; }

		public DateTime LastRoundTS { get; protected set; }
		UInt32 lastLapTickCounter;

		public CarInfo()
		{
			Laps = new BWValue();
			FastestLapTime = new BWValue();
			SlowestLapTime = new BWValue();
			LastLapTime = new BWValue();
			AverageLapTime = new BWValue();
			TotalLapsTime = new BWValue();
			lastLapTickCounter = 0;
		}

		/// <summary>
		/// Adds information of one lap
		/// </summary>
		/// <param name="lapTickCounter"></param>
		public void AddLap(UInt32 lapTickCounter)
		{
			// Do not count the first add (& start signal)
			if (LastRoundTS != DateTime.MinValue)
				Laps.Value++;
			LastRoundTS = DateTime.UtcNow;

			if (lastLapTickCounter != 0)
			{
				LastLapTime.Value = Math.Round((lapTickCounter - lastLapTickCounter) / 1000d, 2);
				if (FastestLapTime.Value == 0 || LastLapTime.Value < FastestLapTime.Value)
					FastestLapTime.Value = LastLapTime.Value;
				if (LastLapTime.Value > SlowestLapTime.Value)
					SlowestLapTime.Value = LastLapTime.Value;

				TotalLapsTime.Value += LastLapTime.Value;
				if (Laps.Value <= 0)
					AverageLapTime.Value = 0;
				else
					AverageLapTime.Value = Math.Round(TotalLapsTime.Value / Laps.Value, 2);

			}
			lastLapTickCounter = lapTickCounter;
		}

		public void Clear()
		{
			Laps.Value = 0;
			FastestLapTime.Value = 0;
			SlowestLapTime.Value = 0;
			LastLapTime.Value = 0;
			AverageLapTime.Value = 0;
			TotalLapsTime.Value = 0;
			lastLapTickCounter = 0;
			LastRoundTS = DateTime.MinValue;
		}
	}

}
