﻿//*************************************************************************************************
//* SlotOMeter - Monitor for slotracing with Carrera digital interface
//* Copyright (C) 2015-2012 by Linus & Joerg Plenert
//*************************************************************************************************
//* Main project: SlotOMeter
//* Sub project :
//*************************************************************************************************
//* This program is free software; you can redistribute it and/or modify
//* it under the terms of the GNU General Public License as published by
//* the Free Software Foundation; either version 2 of the License, or
//* (at your option) any later version.
//*
//* This program is distributed in the hope that it will be useful,
//* but WITHOUT ANY WARRANTY; without even the implied warranty of
//* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//* GNU General Public License for more details.
//*
//* You should have received a copy of the GNU General Public License
//* along with this program; if not, write to the Free Software
//* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//*************************************************************************************************
//* E-Mail / Internet							Snail-Mail
//* Joerg@plenert.net							Joerg Plenert, Roenskenstrasse 69
//* http://www.plenert.net						46562 Voerde, Germany
using System.Collections.Generic;

namespace SlotOMeter
{
	/// <summary>
	/// Best/Worst-Value
	/// </summary>
	public class BWValue
	{
		public double Value { get; set; }
		public bool IsBest { get; protected set; }
		public bool IsWorst { get; protected set; }
		public bool IsNormal { get; protected set; }

		/// <summary>
		/// Compares a list of values and sets the IsBest / IsWorst / IsNormal properties
		/// </summary>
		/// <param name="values"></param>
		/// <param name="highIsBest">true is best is the highest, else best is the lowest value</param>
		static public void DoCompare(List<BWValue> values, bool highIsBest)
		{
			double highestbestValue;
			int hightesIdx;
			double lowestValue;
			int lowestIdx;
			int valueCou;

			valueCou = 0;
			highestbestValue = 0;
			lowestValue = 99999;
			hightesIdx = lowestIdx = -1;

			// Check for highest and lowest values
			for (int i = 0; i < values.Count; i++)
			{
				if (values[i].Value == 0)
					continue;

				valueCou++;

				if (values[i].Value > highestbestValue)
				{
					highestbestValue = values[i].Value;
					hightesIdx = i;
				}

				if (values[i].Value < lowestValue)
				{
					lowestValue = values[i].Value;
					lowestIdx = i;
				}
			}

			// Assign best/worst and normal
			for (int i = 0; i < values.Count; i++)
			{
				values[i].IsBest = values[i].IsWorst = values[i].IsNormal = false;

				// If only one value is valid -> this is the best!
				if (valueCou == 1)
				{
					if (hightesIdx != -1)
						values[hightesIdx].IsBest = true;
					else if (lowestIdx != -1)
						values[lowestIdx].IsBest = true;

					continue;
				}

				if (hightesIdx != -1 && (i == hightesIdx || (values[hightesIdx].Value == values[i].Value && values[i].Value != 0)))
				{
					if (highIsBest)
						values[i].IsBest = true;
					else
						values[i].IsWorst = true;
				}
				else if (lowestIdx != -1 && (i == lowestIdx || (values[lowestIdx].Value == values[i].Value && values[i].Value != 0)))
				{
					if (!highIsBest)
						values[i].IsBest = true;
					else
						values[i].IsWorst = true;
				}
				else
					values[i].IsNormal = (values[i].Value != 0);
			}
		}
	}
}
