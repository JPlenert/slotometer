﻿//*************************************************************************************************
//* SlotOMeter - Monitor for slotracing with Carrera digital interface
//* Copyright (C) 2015-2012 by Linus & Joerg Plenert
//*************************************************************************************************
//* Main project: SlotOMeter
//* Sub project :
//*************************************************************************************************
//* This program is free software; you can redistribute it and/or modify
//* it under the terms of the GNU General Public License as published by
//* the Free Software Foundation; either version 2 of the License, or
//* (at your option) any later version.
//*
//* This program is distributed in the hope that it will be useful,
//* but WITHOUT ANY WARRANTY; without even the implied warranty of
//* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//* GNU General Public License for more details.
//*
//* You should have received a copy of the GNU General Public License
//* along with this program; if not, write to the Free Software
//* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//*************************************************************************************************
//* E-Mail / Internet							Snail-Mail
//* Joerg@plenert.net							Joerg Plenert, Roenskenstrasse 69
//* http://www.plenert.net						46562 Voerde, Germany
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Text;
using System.Threading;

namespace SlotOMeter
{
	public class CUCom
	{
		SerialPort serial;
		CarInfo[] carInfos;
		List<List<BWValue>> valueLists;
		Thread myThread;
		string curComPort;
		bool doExit;

		public string ComPort { get; set; }
		public string StatusText { get; set; }
		public bool IsStatusOK { get; set; }
		public StartInfoEn StartInfo { get; protected set; }
		public DateTime RaceStartTS { get; protected set; }

		public const int CarCount = 6;

		#region Protocol classes
		public enum StartInfoEn
		{
			RaceInProgress = 0,
			StartAnnounced = 1,
			GetReady1 = 2,
			GetReady2 = 3,
			GetReady3 = 4,
			GetReady4 = 5,
			GetReady5 = 6,
			GetReady6 = 7,
			FailStart1 = 8,
			FailStart2 = 9,
		}

		enum FuelModeEn
		{
			Off = 0,
			Normal = 1,
			Real = 2,
		}

		class CUInfo
		{
			public int[] FuelStatus { get; protected set; }
			public StartInfoEn StartInfo { get; protected set; }
			public FuelModeEn FuelMode { get; protected set; }
			public bool IsPitlaneAvaiable { get; protected set; }
			public bool IsLapcounterAvaiable { get; protected set; }

			public CUInfo(byte[] data)
			{
				FuelStatus = new int[CarCount];
				// 2..7 -> Fuel status
				for (int i = 0; i < 6; i++)
					FuelStatus[i] = data[i + 2] & 0x0F;
				// 8..9 -> unknown
				// 10 -> StartInfo
				StartInfo = (StartInfoEn)(data[10] & 0x0F);
				// 11 -> FuelMode / PitLane / Lapcounter
				FuelMode = (FuelModeEn)(data[10] & 0x03);
				IsPitlaneAvaiable = (data[10] & 0x4) != 0;
				IsLapcounterAvaiable = (data[10] & 0x8) != 0;
				// 12..13 -> Can be fuled (bitmask)
				// 14 -> Count of cars on tower?
			}
		}

		class RoundInfo
		{
			public int CarID { get; protected set; }
			public UInt32 Timer { get; protected set; }
			public int Sensor { get; protected set; }

			public RoundInfo(byte[] data)
			{
				// 2 -> CarID
				CarID = data[1] & 0x0F;
				// 3..8 -> Timer
				Timer = ((UInt32)(data[2] & 0x0F) << 24) |
					((UInt32)(data[3] & 0x0F) << 28) |
					((UInt32)(data[4] & 0x0F) << 16) |
					((UInt32)(data[5] & 0x0F) << 20) |
					((UInt32)(data[6] & 0x0F) << 8) |
					((UInt32)(data[7] & 0x0F) << 12) |
					((UInt32)(data[8] & 0x0F) << 0) |
					((UInt32)(data[9] & 0x0F) << 4);
				Sensor = data[10] & 0x0f;
			}
		}
		#endregion

		/// <summary>
		/// Handles a command; Sends it to the CU and receives the answer
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		byte[] HandleCommand(char command)
		{
			byte[] inData;
			int bytesToRead;
			int curByte = 0;

			// Delete Garbage
			serial.ReadExisting();

			// Send command
			serial.Write("\"" + command);

			// Define lenght depending on the command
			if (command == '0')
				bytesToRead = 1 + 4 + 2;
			else if (command == '?')
				bytesToRead = 1 + 10 + 2;
			else
			{
				StatusText = "Unknown request";
				IsStatusOK = false;
				return null;
			}

			inData = new byte[bytesToRead];

			// Read answer
			try
			{
				int inByte;

				do
				{
					inByte = serial.ReadByte();
					inData[curByte] = (byte)inByte;
					curByte++;

					// Anwer on "?" command is another that we wanted. Change lenght
					if (command == '?' && curByte == 2 && inData[1] == ':')
					{
						bytesToRead = 1 + 14 + 2;
						inData = new byte[bytesToRead];
						inData[0] = (byte)'?';
						inData[1] = (byte)inByte;
					}

				} while (inData.Length != curByte);
			}
			catch
			{
				if (curByte == 0)
					StatusText = "No answer";
				else
					StatusText = "Invalid answer -> length";
				IsStatusOK = false;
				return null;
			}

			// Must end in '$'
			if (inData[inData.Length-1] != '$')
			{
				StatusText = "Invalid answer -> no $";
				IsStatusOK = false;
				return null;
			}

			// Checksum
			int checksum = 0;
			for (int idx = 1; idx<inData.Length-2; idx++)
				checksum += inData[idx] & 0x0F;
			checksum &= 0x0f;
			checksum |= 0x30;

			if (inData[inData.Length-2] != checksum)
			{
				StatusText = "Invalid answer -> wrong checksum";
				IsStatusOK = false;
				return null;
			}

			return inData;			
		}

		public CUCom()
		{
			valueLists = new List<List<BWValue>>();

			for (int i=0; i<5; i++)
				valueLists.Add(new List<BWValue>());

			carInfos = new CarInfo[CarCount];
			for (int i = 0; i < CarCount; i++)
			{
				carInfos[i] = new CarInfo();
				valueLists[0].Add(carInfos[i].Laps);
				valueLists[1].Add(carInfos[i].LastLapTime);
				valueLists[2].Add(carInfos[i].FastestLapTime);
				valueLists[3].Add(carInfos[i].SlowestLapTime);
				valueLists[4].Add(carInfos[i].AverageLapTime);
			}

			myThread = new Thread(Worker);
			myThread.Start();
		}

		/// <summary>
		/// Returns the CarInfo of a given carID
		/// </summary>
		/// <param name="carID">0..5</param>
		/// <returns></returns>
		public CarInfo GetCarInfo(int carID)
		{
			return carInfos[carID];
		}

		/// <summary>
		/// Resets the values of all CarInfos
		/// </summary>
		public void ResetValues()
		{
			for (int i = 0; i < CarCount; i++)
				carInfos[i].Clear();
			CalcOverlappingValues();
		}

		void CalcOverlappingValues()
		{
			BWValue.DoCompare(valueLists[0], true);
			BWValue.DoCompare(valueLists[1], false);
			BWValue.DoCompare(valueLists[2], false);
			BWValue.DoCompare(valueLists[3], false);
			BWValue.DoCompare(valueLists[4], false);

			// Calc Rank
			List<CarInfo> rankList = new List<CarInfo>(carInfos);
			rankList.Sort((x, y) =>
			{
				int ret;

				ret = (int)(y.Laps.Value - x.Laps.Value);
				if (ret == 0)
					ret = (int)((x.LastRoundTS - y.LastRoundTS).TotalMilliseconds);

				return ret;
			});

			for (int i = 0; i < CarCount; i++)
			{
				if (rankList[i].Laps.Value != 0)
					rankList[i].Rank = i + 1;
				else
					rankList[i].Rank = 0;
			}
		}

		public JObject GetJson()
		{
			JObject jRoot = new JObject();

			jRoot["isOk"] = IsStatusOK;
			jRoot["statusText"] = StatusText;
			jRoot["startInfo"] = StartInfo.ToString();
			jRoot["raceStartTS"] = RaceStartTS;

			JArray jCarAarry = new JArray();
			jRoot["cars"] = jCarAarry;

			for (int car = 0; car < CarCount; car++)
			{
				JObject jCar = new JObject();
				jCarAarry.Add(jCar);
				jCar["laps"] = (int)carInfos[car].Laps.Value;
				jCar["rank"] = carInfos[car].Rank;
				jCar["averageLapTime"] = carInfos[car].AverageLapTime.Value;
				jCar["fastestLapTime"] = carInfos[car].FastestLapTime.Value;
				jCar["slowestLapTime"] = carInfos[car].SlowestLapTime.Value;
				jCar["lastLapTime"] = carInfos[car].LastLapTime.Value;
				jCar["lastRoundTime"] = carInfos[car].LastRoundTS;
				jCar["totalTime"] = carInfos[car].TotalLapsTime.Value;
			}

			return jRoot;
		}

		void Worker()
		{
			while (!doExit)
			{
				// Reopen the port if requestes ComPort changes
				if (ComPort != curComPort)
				{
					if (serial != null)
						serial.Close();

					curComPort = ComPort;
					try
					{
						serial = new SerialPort(curComPort, 19200);
						serial.ReadTimeout = 500;
						serial.Open();
					}
					catch
					{
						serial = null;
						StatusText = "Invalid Com-Port";
						IsStatusOK = false;
					}
				}

				if (serial == null)
				{
					Thread.Sleep(1000);
					continue;
				}

				byte[] inData;

				// Get round information
				inData = HandleCommand('?');
				if (inData != null)
				{
					Debug.WriteLine(ASCIIEncoding.ASCII.GetString(inData));
					if (inData[1] == (byte)':')
					{
						CUInfo cuInfo = new CUInfo(inData);
						StartInfo = cuInfo.StartInfo;

						// Reset values on "Start" pressed on CU
						if (cuInfo.StartInfo == CUCom.StartInfoEn.StartAnnounced && RaceStartTS != DateTime.MinValue)
							ResetValues();
					}
					else
					{
						RoundInfo roundInfo = new RoundInfo(inData);

						// Start race
						if (RaceStartTS == DateTime.MinValue)
							RaceStartTS = DateTime.UtcNow;

						// Add info
						carInfos[roundInfo.CarID-1].AddLap(roundInfo.Timer);
						CalcOverlappingValues();
					}

					StatusText = "OK";
					IsStatusOK = true;
				}

				Thread.Sleep(50);
			}
		}

		public void Exit()
		{
			//myThread.Abort();
			doExit = true;
		}
	}
}
