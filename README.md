# README #
SlotOMeter is a monitor (aka intelligent lap counter) for the digital slot racing system of Carrera.

Copyright 2015-2016 by Linus & Joerg Plenert (http://www.plenert.net / http://bitbucket.org/JPlenert/slotometer)

I developed the software together with my oldest son, just because we did not want to wait till an ordered PC-adapter and a software gets delivered.
## How it works ##
The digital slot racing systems of Carrera come together with a CU (Control-Unit). The connector for the “PC-Unit” is a simple serial port with TTL signals (aka TTL-RS232). SlotOMeter uses that connector to communication with the system. You can’t use Carrera black box or a lap counter, you need a CU to do this!
## How to use the software ##
### Setup ###
The software is quit very small. Just put the EXE-File somewhere (maybe on your desktop) and start it. No installation is needed, but Microsoft .Net Framework 4.5 must be installed on your PC. If the main window comes up, at the bottom of window there is a combo box where you can choose the COM-Port. Just right to the box there is a status that shows you if the connection is OK.
### Usage ###
To Reset all counters, press the button “reset” or start a new race at your CU. Your cars should start before the “Start/Finish” line, so they drive over the line to start the race. You can collapse unneeded cars by pressing the button with the number on it.
## Hardware ##
There are a lot of cables on the market to connect that to your PC via a USB-Port. Also the original Carrera (PC-Unit) does the same. That “cables” have an integrated USB-To-Serial converter (usually from Prolific or FTDI) that does install a COM-Port on your PC.
### Homemade cable ###
You can also easily build you own cable. The connector at the CU is the same as on old PS/2 keyboards. So you can use an old keyboard and cut off the cable. There are a lot of USB-to-TTL-serial converters on the market (e.g. FTDI TTL-232R-RPI or DELOCK 83116). Putting this together and you have your own cable. You also can use a MAX 232 / 3232 chip and build up a connection, because there is +5V also on the connector of the CU.
Arrangement the connector:

* 1 – RX data
* 2 – TX data
* 4 - +5V
* 5 – GND

## Links ##
There is a very good documentation of the CU and it's protocol that helped us a lot building this software: [http://www.slotbaer.de/index.php/carrera-digital-124-132/10-cu-rundenzaehler-protokoll]

## Problems ##
Q: The laps are not counted or the laps will be counted very late.
A: Switch of the CU off and on while the software is running.

## License ##

Copyright 2015-2016 by Linus & Joerg Plenert (www.plenert.net)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.