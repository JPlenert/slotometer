﻿using SlotOMeter;
using System;
using System.Threading;

namespace SlotOMeterCUServer
{
    class Program
    {
        static void Main(string[] args)
        {
            var com = new CUCom();
            com.ComPort = "/dev/ttyO4";

            WebServer ws = new WebServer(com);
            ws.Start();

            while (true)//(!Console.KeyAvailable)
            {
                if (com.IsStatusOK)
                {
                    var ci = com.GetCarInfo(0);
                    Console.WriteLine("OK;  {0} - {1}", ci.Laps.Value, ci.LastLapTime.Value);

                }
                else
                    Console.WriteLine("ERR; {0} - {1}", com.StartInfo.ToString(), com.StatusText);
                Thread.Sleep(1000);
            }

            ws.Stop();
            com.Exit();
        }
    }
}
