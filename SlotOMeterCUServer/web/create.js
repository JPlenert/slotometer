var deletebtn =  document.getElementById("delete");
var addbtn = document.getElementById("add");
var minneededColums = 2;
var maxneededColums = 7;
var neededRows = 8;
var neededColums = 7;
var parent = document.getElementById("list");


document.onload = create(neededColums,neededRows);


function create(neededColums,neededRows){      /* creates the page with flexboxes*/
if (neededColums == maxneededColums){
    addbtn.style.visibility = "hidden";
}
else{
    addbtn.style.visibility = "visible";
}
if (neededColums == minneededColums){
    deletebtn.style.visibility = "hidden";
}
else{
    deletebtn.style.visibility = "visible";
}
/* On press of button delete change this nummver to decrese the colums also add variable for the ratio of the element*/ 

    var flex = 84/neededColums; /** 84 weil ein bisschen überschuss sein muss damit nicht alle flex elemente direkt ohne striche anneinander sind */
    var flex = flex+"%";
    var ul = document.getElementById("list");
   
    for (row= 0;neededRows>row;row++) {
    for (column = 0; neededColums> column; column++){
        if (column==0 && row==0) {
            createHtmlElement(ul,row,column,"flex-empty",null,flex)
        }
        else if(column>0 && column<7&& row==0||column==0 && row!=0){
            if(column>0 && column<7){
                createHtmlElement(ul,row,column,"flex-header","Auto "+column,flex);
            }
            else{
                switch(row){
                    case 1:
                        createHtmlElement(ul,row,column,"flex-header","Round",flex);
                        break;
                    case 2:
                        createHtmlElement(ul,row,column,"flex-header","Place",flex);
                        break;
                    case 3:
                        createHtmlElement(ul,row,column,"flex-header","Last Round Time",flex);
                        break;
                    case 4:
                        createHtmlElement(ul,row,column,"flex-header","Fastest Round Time",flex);
                        break;
                    case 5:
                        createHtmlElement(ul,row,column,"flex-header","Average Round Time",flex);
                        break;
                    case 6:
                        createHtmlElement(ul,row,column,"flex-header","Slowest Round Time",flex);
                        break;
                    case 7:
                        createHtmlElement(ul,row,column,"flex-header","Total Time Raced",flex);
                        break;
                }        
            
            }
        }
        else{
            if(row == 1){
                createHtmlElement(ul,row,column,"flex-item",0,flex);
            }
            else if (row == 2){
                createHtmlElement(ul,row,column,"flex-item",0+".",flex);
            }
            else{
                createHtmlElement(ul,row,column,"flex-item","0 sec",flex);
            }
    }
    }
}
}
function createHtmlElement(ul,row,column,itemtype,content,flex){
    var createElement = document.createElement("li");
    createElement.setAttribute("Id",String.fromCharCode(row+1 + 64)+" "+column)
    createElement.setAttribute("class",itemtype);
    createElement.innerHTML = content;
    ul.appendChild(createElement);
    createElement.style.flexBasis = flex;
}


deletebtn.addEventListener("click",function(){
    while(parent.firstChild ){
        parent.removeChild( parent.firstChild );
      }
    if (minneededColums <= neededColums-1){
    neededColums = neededColums-1;

    }
    create(neededColums,neededRows);
    /*delete last elements from list */ 
})

addbtn.addEventListener("click",function(){
    while(parent.firstChild ){
        parent.removeChild( parent.firstChild );
      }
      if (maxneededColums >= neededColums+1){
        neededColums = neededColums+1;
    }
    create(neededColums,neededRows);
})
