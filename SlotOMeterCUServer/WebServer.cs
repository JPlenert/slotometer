﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using SlotOMeter;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Text;

namespace SlotOMeterCUServer
{
    class WebServer : IStartup
    {
        IWebHost host;
        CUCom cuCom;

        Dictionary<string, string> webFiles;

        public WebServer(CUCom cuCom)
        {
            this.cuCom = cuCom;

            // Find and load web files 
            webFiles = new Dictionary<string, string>();
            string dirPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            dirPath = Path.Combine(dirPath, "web");
            if (!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);
            foreach (var file in Directory.GetFiles(dirPath))
                webFiles[Path.GetFileName(file)] = File.ReadAllText(file);
        }

        public void Start()
        {
            var urls = $"http://*:80";
            host = new WebHostBuilder()
                .UseKestrel()
                .UseUrls(urls)
                .ConfigureServices(serv => { serv.AddSingleton(typeof(IStartup), this); })
                .Build();
            host.Start();
        }

        public void Stop()
        {
            host.StopAsync().Wait();
        }

        public void Configure(IApplicationBuilder app)
        {
            app.Use(async (context, next) => await RequestHandler(context));
        }

        private async Task RequestHandler(HttpContext context)
        {
#if DEBUG
            // For development to allow usage with Visual Studio Code http-Server
            context.Response.Headers.Append("Access-Control-Allow-Origin", "*");
#endif
            bool answered = false;

            if (context.Request.Method == "GET")
            {

                if (context.Request.Path == "/")
                {
                    await context.Response.WriteAsync(webFiles["index.html"]);
                    answered = true;
                }
                else if (context.Request.Path == "/race.json")
                {
                    await context.Response.WriteAsync(cuCom.GetJson().ToString());
                    answered = true;
                }
                else
                {
                    // Get filename and cut off first "/"
                    if (webFiles.TryGetValue(context.Request.Path.Value.Substring(1), out string content))
                    {
                        await context.Response.WriteAsync(content);
                        answered = true;
                    }
                }
            }

            if (!answered)
                context.Response.StatusCode = 404;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services) => services.BuildServiceProvider();
    }
}
