﻿//*************************************************************************************************
//* SlotOMeter - Monitor for slotracing with Carrera digital interface
//* Copyright (C) by Linus & Joerg Plenert
//*************************************************************************************************
//* Main project: SlotOMeter
//* Sub project :
//*************************************************************************************************
//* This program is free software; you can redistribute it and/or modify
//* it under the terms of the GNU General Public License as published by
//* the Free Software Foundation; either version 2 of the License, or
//* (at your option) any later version.
//*
//* This program is distributed in the hope that it will be useful,
//* but WITHOUT ANY WARRANTY; without even the implied warranty of
//* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//* GNU General Public License for more details.
//*
//* You should have received a copy of the GNU General Public License
//* along with this program; if not, write to the Free Software
//* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//*************************************************************************************************
//* E-Mail / Internet							Snail-Mail
//* Joerg@plenert.net							Joerg Plenert, Roenskenstrasse 69
//* http://www.plenert.net						46562 Voerde, Germany
//*************************************************************************************************
using SlotOMeter;
using System;
using System.Windows.Controls;
using System.Windows.Media;

namespace SlotOMeterWPF
{
	/// <summary>
	/// Interaction logic for UserControl1.xaml
	/// </summary>
	partial class CarView : UserControl
	{
		CarInfo carInfo;
		double oldWidth;
		internal bool isClosed;

		internal CarView(CarInfo carInfo, int carID)
		{
			this.carInfo = carInfo;

			InitializeComponent();

			lbLapCount.Content = null;
			lbLastLapTime.Content = null;
			lbFastestLapTime.Content = null;
			lbSlowestLapTime.Content = null;
			lbAverageLapTime.Content = null;
			lbRank.Content = null;

			pbLap.Content = carID+1;

			pbLap.Click += PbLap_Click;
		}

		private void PbLap_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			if (isClosed)
			{
				Width = oldWidth;
				isClosed = false;
			}
			else
			{
				oldWidth = Width;
				Width = pbLap.Width;
				isClosed = true;
			}
		}

		void SetValue(Label label, BWValue value, bool twoDigits)
		{
			if (twoDigits)
				label.Content = String.Format("{0:0.00}", value.Value);
			else
				label.Content = String.Format("{0}", value.Value);

			if (value.IsBest)
				label.Foreground = Brushes.Green;
			else if (value.IsWorst)
				label.Foreground = Brushes.Red;
			else if (value.IsNormal)
				label.Foreground = Brushes.Orange;
			else
				label.Foreground = Brushes.Black;
		}

		public void UpdateInfo()
		{
			SetValue(lbLapCount, carInfo.Laps, false);
			SetValue(lbLastLapTime, carInfo.LastLapTime, true);
			SetValue(lbFastestLapTime, carInfo.FastestLapTime, true);
			SetValue(lbSlowestLapTime, carInfo.SlowestLapTime, true);
			SetValue(lbAverageLapTime, carInfo.AverageLapTime, true);
			if (carInfo.Rank == 0)
				lbRank.Content = null;
			else
				lbRank.Content = carInfo.Rank;
		}
	}
}
