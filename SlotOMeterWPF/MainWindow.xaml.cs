﻿//*************************************************************************************************
//* SlotOMeter - Monitor for slotracing with Carrera digital interface
//* Copyright (C) by Linus & Joerg Plenert
//*************************************************************************************************
//* Main project: SlotOMeter
//* Sub project :
//*************************************************************************************************
//* This program is free software; you can redistribute it and/or modify
//* it under the terms of the GNU General Public License as published by
//* the Free Software Foundation; either version 2 of the License, or
//* (at your option) any later version.
//*
//* This program is distributed in the hope that it will be useful,
//* but WITHOUT ANY WARRANTY; without even the implied warranty of
//* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//* GNU General Public License for more details.
//*
//* You should have received a copy of the GNU General Public License
//* along with this program; if not, write to the Free Software
//* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//*************************************************************************************************
//* E-Mail / Internet							Snail-Mail
//* Joerg@plenert.net							Joerg Plenert, Roenskenstrasse 69
//* http://www.plenert.net						46562 Voerde, Germany
using SlotOMeter;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace SlotOMeterWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    partial class MainWindow : Window
	{
		CUCom cuCom;
		List<CarView> carViewList;
		DispatcherTimer timer;
		Size initialWindowsSize = Size.Empty;
		
		public MainWindow()
		{
			InitializeComponent();

			cuCom = new CUCom();

			carViewList = new List<CarView>();
			for (int i = 0; i < CUCom.CarCount; i++)
			{
				CarView carView = new CarView(cuCom.GetCarInfo(i), i);
				carView.SizeChanged += CarView_SizeChanged;
				carViewList.Add(carView);
				carStack.Children.Add(carView);
			}
			
			pbReset.Click += PbReset_Click;
			pbInfo.Click += PbInfo_Click;
			SizeChanged += MainWindow_SizeChanged;

			timer = new DispatcherTimer();
			timer.Interval = new TimeSpan(500);
			timer.Tick += Timer_Tick;
			timer.Start();


			// Add all existing COM-Ports to the combo (but sorted)
			List<string> comNames = new List<string>(System.IO.Ports.SerialPort.GetPortNames());
			comNames.Sort();

			foreach (string com in comNames)
				coComPort.Items.Add(com);

			// Select the first if possible
			if (comNames.Count > 0)
			{
				coComPort.SelectedItem = comNames[0];
				CoComPort_DropDownClosed(null, null);
			}

			coComPort.DropDownClosed += CoComPort_DropDownClosed;		
		}

		private void CarView_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			CalcScale(new Size(ActualWidth, ActualHeight));
		}

		private void MainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			if (initialWindowsSize == Size.Empty)
			{
				initialWindowsSize = e.NewSize;
				SizeToContent = SizeToContent.Manual;
			}

			CalcScale(e.NewSize);
		}

		void CalcScale(Size newSize)
		{
			int openedLanes = 0;
			foreach (CarView carView in carViewList)
				openedLanes += carView.isClosed ? 0 : 1;

			ApplicationScaleTransform.ScaleX = newSize.Width / (initialWindowsSize.Width * (openedLanes / 6d) + (6 - openedLanes) * 17);
			ApplicationScaleTransform.ScaleY = (newSize.Height - ControlBar.ActualHeight*2) / (initialWindowsSize.Height - ControlBar.ActualHeight*2);
		}

		private void PbInfo_Click(object sender, RoutedEventArgs e)
		{
			string info = "SlotOMeter - Monitor for slotracing with Carrera digital interface\n\n" +
				"Version #VERSION#\n\n" +
				"Further info: http://bitbucket.org/JPlenert/SlotOMeter\n\n" +
				"Copyright(C) 2015-2016 by Linus & Joerg Plenert\n\n" +
				"This program is free software: you can redistribute it and / or modify\n"+
				"it under the terms of the GNU General Public License as published by\n"+
				"the Free Software Foundation, either version 3 of the License, or\n"+
				"(at your option) any later version.\n"+
				"This program is distributed in the hope that it will be useful,\n"+
				"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"+
				"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the\n"+
				"GNU General Public License for more details.\n"+
				"You should have received a copy of the GNU General Public License\n"+
				"along with this program.If not, see <http://www.gnu.org/licenses/>.\n";

			info = info.Replace("#VERSION#", Assembly.GetExecutingAssembly().GetName().Version.ToString());

			MessageBox.Show(info, "SlotOMeter - Information");
		}

		private void CoComPort_DropDownClosed(object sender, EventArgs e)
		{
			cuCom.ComPort = coComPort.Text; 
		}

		private void PbReset_Click(object sender, RoutedEventArgs e)
		{
			cuCom.ResetValues();
		}

		private void Timer_Tick(object sender, EventArgs e)
		{
			foreach (CarView carView in carViewList)
				carView.UpdateInfo();

			lbCominfo.Foreground = cuCom.IsStatusOK ? Brushes.DarkGreen : Brushes.Red;
			lbCominfo.Content = cuCom.StatusText;
		}

		protected override void OnClosed(EventArgs e)
		{
			cuCom.Exit();
		}
	}
}
